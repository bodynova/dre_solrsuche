# dre_SolrSuche

Bender Solr Suche Composer Modul


### JavaScript Frameworks

Link: 

Underscore:
https://underscorejs.org

Backbone:
https://github.com/jashkenas/backbone.git




## Installation mit composer:

dem bitbucket repository rechte seitens bodynova einräumen.

anschließend. Das composer repo configurieren:

```php
config repositories.bodynova/dre_solrsuche git git@bitbucket.org:bodynova/dre_solrsuche.git
```

anschließend kann das git repo hinzugefügt werden mit:

```php
composer require bender/dre_solrsuche
// bei einem bestimmten Versionsupdate am Ende ein :"1.0.0"
hinzufügen.
```

und mit composer update den Rest aktualisieren

