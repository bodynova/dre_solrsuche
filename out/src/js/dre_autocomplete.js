$(document).ready(function () {
	//console.log('aus autocomplete:');
	//console.log(lang);
	var options = {
		/**
		 * auswertung der Usereingabe und absetzen des Http_GET Request an form.php
		 * zuvor validierung der Sprachauswahl de als Standarteinstellung berücksichtigt.
		 */
		url: function(phrase) {
			// feststellen der Sprache
			// $('.languages-menu li').each(function(){if($(this).hasClass('active')){console.log($(this).children()[0].classList[1]);}});
			var sprache = $('#sprache').val();
			return "https://sales.bodynova.com:8983/solr/bodyconnect/select?q=*" + phrase + "*&indent=on&wt=json";
		},
		
		ajaxSettings: {
			dataType: "json"
		},
		
		listLocation: function (response) {
			return response.response.docs
		},
		
		theme: "solid",
		/**
		 * Value des Search Fields bis eingabe vorgenommen wird
		 */
		placeholder: "Suchbegriff eingeben ...",
		/**
		 * Value des Search Fields, wenn eine Auswahl getroffen wurde
		 */
		getValue: function(response){
			//var res = jQuery.parseJSON(response);
			//console.log(response.titel_autocomplete[0]);
			return response.titel_autocomplete[0];
		},//"OXARTNUM",
		/**
		 * Einstellung: Wo der Suchbegriff mit dem Ergebnis matched, wird dies hervorgehoben
		 */
		highlightPhrase: true,
		/**
		 * Zeitverzögerung bis die Abfrage abgeschickt wird bei Usereingabe
		 */
		requestDelay: 500,
		/**
		 * Anzahl der Mindestzeichen bis die Suche ausgelöst wird
		 */
		minCharNumber: 3,
		/**
		 * Objekt list Array von Objekten für die Auswahlliste
		 */
		list:{
			maxNumberOfElements: 30,
			/**
			 * Objekt für die Animation bei der Darstellung der Liste
			 * */
			showAnimation: {
				type: "fade", //normal|slide|fade
				time: 400,
				callback: function() {
				
				}
			},
			/**
			 * Trigger der ausgelöst wird, wenn die Liste geladen wird
			 * */
			onLoadEvent:function(){
			
			},
			/**
			 *
			 * Trigger der ausgelöst wird, wenn der User eine Auswahl getroffen hat
			 *
			 * */
			onClickEvent: function () {
				/*
				var oxid   = $("#search").getSelectedItemData().OXID;
				var title  = $("#search").getSelectedItemData().Title;
				var artnum = $("#search").getSelectedItemData().OXARTNUM;
				var lang   = $('#sprache').val();
				
				// DEBUG:
				console.log(artnum + " | "  + title); //+ oxid + " | "
				
				$.ajax({
					type:'get',
					url:'form.php',
					data:{
						fnc  : 'getArtikel',
						OXID : oxid,
						lang : lang
					},
					beforeSend:function(){
						//launchpreloader();
						//DEBUG:
						//console.log(item);
					},
					complete:function(){
						//stopPreloader();
						//parseMD();
					},
					success:function(result){
						// DEBUG:
						//console.log(result);
						console.log('Sprache: ' + lang);
						//console.log(result);
						var res = jQuery.parseJSON(result);
						console.log(res);
						
						var text = null;
						
						if( lang == 0 ){
							text = res.Artikeltext['OXLONGDESC'];
						}else if( lang == 1 ){
							text = res.Artikeltext['OXLONGDESC_1'];
						}else if( lang == 2 ){
							text = res.Artikeltext['OXLONGDESC_2'];
						}
						/*$('#oxid').val();
						$('#oxid').val(res.Artikel['OXID']);
						$('#md').html('');
						$('#md').html(text);
						//parseMD();
						*/
				//}
				
				//});
				//parseMD();
			}
		},
		/**
		 *
		 * Template Objekt als Array von Objekten
		 *
		 * */
		template: {
			type: "custom",
			method: function(value, item) {
				// den Preis formatieren
				var price = accounting.formatMoney(item.oxprice, "€", 2, ".", ",","%v %s");
				/*
				console.log('preis:');
				console.log(price);
				console.log(item);
				//console.log(value);
				*/
				var string = "<a href=\"" + item.oxseourl_de + "\"><img src='https://bodynova.de/out/pictures/generated/product/1/87_87_100/"+ item.oxpic1 +"' width='50px' height='50px'>"+
					"<span class='ArtTitle'>" + value + "</span>" +
				"<br/><span class='small'>Artikelnr: " + item.oxartnum + "</span>"+
				"<span class='pull-right'>" + price + "</span></a>";
				return string ;//item.OXARTNUM + " | "  + item.Title ; //+ item.OXID + " | "
			}
		}
		
	};
	$("#searchParam").easyAutocomplete(options);
});
