<?php
namespace Bender\dre_SolrSuche\Core;
class Config extends \OxidEsales\Eshop\Core\Config{}

namespace Bender\dre_SolrSuche\Application\Model;
class dre_solrsuche extends \OxidEsales\Eshop\Application\Model\Search{}
class dre_ArticleList extends \OxidEsales\Eshop\Application\Model\ArticleList{}

namespace Bender\dre_SolrSuche\Application\Controller;
class dre_SolrSuchController extends \OxidEsales\Eshop\Application\Controller\FrontendController{}
