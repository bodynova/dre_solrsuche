<?php

namespace Bender\dre_SolrSuche\Application\Controller;


use OxidEsales\Eshop\Core\Registry;

class dre_SolrSuchController extends \OxidEsales\Eshop\Application\Controller\FrontendController
{

    protected $_sClass = 'dre_solrsuchecontroller';
    /**
     * Current class template name.
     *
     * @var string
     */
    protected $_sThisTemplate = 'dre_searchresult.tpl';

    protected $suchurl = '';
    /**
     * @return null
     */
    public function render(){
        return parent::render();
    }

    public function suche(){
        $searchurl = Registry::getConfig()->getConfigParam('dre-search-url');
        $searchcol = Registry::getConfig()->getConfigParam('dre-search-col');
        $this->suchurl = $searchurl . $searchcol . '/select';

        $explodiert = explode(' ', str_replace('%20', ' ', $_GET['phrase']));
        $searchkey = implode(' ', $explodiert);



        // suche nach artikelnummer und ausgabe bei einem einzigen Ergebnis.
        $suchresult = $this->getSearchArtnum($searchkey);
        $sucherg = json_decode($suchresult);


        if($this->getSearchCount($sucherg) == 1){
            echo json_encode($this->verarbeiteErgebnis($sucherg));
            die();
        }

        // suche nach genauer entsprechung und ausgabe
        $suchresult = $this->getSearchStrict($searchkey);
        $sucherg = json_decode($suchresult);
        #d($suchresult);
        #die();

        if ($this->getSearchCount($sucherg) > 1) {
            $arrArtikelIds = array();
            foreach ($sucherg->response->docs as $artikel) {
                $arrArtikelIds[] = $artikel->id;
            }
            $arrRueckgabe['artikelids'] = $arrArtikelIds;
            foreach ($arrArtikelIds as $artikelid) {
                $articleobj = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
                $transarticle = $articleobj;
                $transarticle->enablePriceLoad();

                $transarticle->load($artikelid);
                if ($transarticle->isVariant()) {
                    $transarticle->getParentArticle();
                }
                if ($transarticle->__get('oxarticles__oxactive')->rawValue == 1) {
                    $transarticle->enablePriceLoad();
                    $transarticle->artnum = $transarticle->__get('oxarticles__oxartnum')->rawValue;
                    $transarticle->getMinPrice();
                    $transarticle->getTPrice();
                    $transarticle->preis = $transarticle->getFVarMinPrice();
                    $link = $transarticle->getLink();
                    $transarticle->pic = $transarticle->getIconUrl();
                    $transarticle->link = $link;
                    $arrRueckgabe['artikelobjekte'][] = $transarticle;
                }
            }

            header('Content-Type: application/json');
            Registry::getUtils()->showJsonAndExit($arrRueckgabe);
            //echo json_encode($arrRueckgabe);
            die();
        }


        // wenn keine Eindeutigkeit gefunden wurde add fuzzy und leventstein
        $searchkey = implode('~ ', $explodiert) . '~';

        $suchresult = $this->getSearchResults($searchkey);
        $sucherg = json_decode($suchresult);

        // wenn nix gefunden wurde weitere Verarbeitung abbrechen und log in Datei
        $arrRueckgabe['anzahlGefunden'] = $this->getSearchCount($sucherg);

        if ($this->getSearchCount($sucherg) == 0) {
            $file = fopen(dirname(__DIR__)."/nixgefunden.txt", "a+");
            fwrite($file, $searchkey."\n");
            fclose($file);
            json_encode($arrRueckgabe);
            die();
        }
        $file = fopen(dirname(__DIR__) . "/sucheingabe.txt", "a+");
        fwrite($file, $searchkey .' Ergebnisse:'. $this->getSearchCount($sucherg). "\n");
        fclose($file);

        $arrArtikelIds = array();
        foreach ($sucherg->response->docs as $artikel){
            $arrArtikelIds[] = $artikel->id;
        }
        $arrRueckgabe['artikelids'] = $arrArtikelIds;
        foreach ($arrArtikelIds as $artikelid){
            $articleobj = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
            $transarticle = $articleobj;
            $transarticle->enablePriceLoad();

            $transarticle->load($artikelid);
            if($transarticle->isVariant()){
                $transarticle->getParentArticle();
            }
            if($transarticle->__get('oxarticles__oxactive')->rawValue == 1){
                $transarticle->enablePriceLoad();
                $transarticle->artnum = $transarticle->__get('oxarticles__oxartnum')->rawValue;
                $transarticle->getMinPrice();
                $transarticle->getTPrice();
                $transarticle->preis = $transarticle->getFVarMinPrice();
                $link = $transarticle->getLink();
                $transarticle->pic = $transarticle->getIconUrl();
                $transarticle->link = $link;
                $arrRueckgabe['artikelobjekte'][] = $transarticle;
            }
        }
        header('Content-Type: application/json');
        Registry::getUtils()->showJsonAndExit($arrRueckgabe);
        //echo json_encode($arrRueckgabe);
    }

    public function getSearchResults($searchkey){
        return file_get_contents($this->suchurl.'?q=' . urlencode($searchkey).  '&indent=on&wt=json');
    }

    public function getSearchArtnum($searchkey){
        return file_get_contents($this->suchurl . '?q=childartnums_txt:' . urlencode($searchkey) . '&indent=on&wt=json');
    }

    public function getSearchStrict($searchkey){
        return file_get_contents($this->suchurl . '?q=' . urlencode($searchkey) . '&indent=on&wt=json');
    }

    public function getSearchCount($sucherg){
        return $sucherg->response->numFound;
    }

    public function test(){
        $phrase = $_GET['phrase'];
        echo file_get_contents($this->suchurl."?q=" . $phrase.  "&indent=on&wt=json");
    }
    public function getArticleById($oxid){
        $articleobj = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
        return $articleobj->load($oxid);
    }

    public function verarbeiteErgebnis($sucherg){
        $arrArtikelIds = array();
        foreach ($sucherg->response->docs as $artikel) {
            $arrArtikelIds[] = $artikel->id;
        }
        $arrRueckgabe['artikelids'] = $arrArtikelIds;
        foreach ($arrArtikelIds as $artikelid) {
            $articleobj = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
            $transarticle = $articleobj;
            $transarticle->enablePriceLoad();

            $transarticle->load($artikelid);
            if ($transarticle->isVariant()) {
                $transarticle->getParentArticle();
            }
            if ($transarticle->__get('oxarticles__oxactive')->rawValue == 1) {
                $transarticle->enablePriceLoad();
                $transarticle->artnum = $transarticle->__get('oxarticles__oxartnum')->rawValue;
                $transarticle->getMinPrice();
                $transarticle->getTPrice();
                $transarticle->preis = $transarticle->getFVarMinPrice();
                $link = $transarticle->getLink();
                $transarticle->pic = $transarticle->getIconUrl();
                $transarticle->link = $link;
                $arrRueckgabe['artikelobjekte'][] = $transarticle;
            }
            return $arrRueckgabe;
        }
    }

}
