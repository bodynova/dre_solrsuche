[{include file="dre_autocomplete_bottom.tpl"}]

[{if $oView->showSearch()}]
    <form class="form search" role="search" action="[{$oViewConf->getSelfActionLink()}]" method="get" name="search">
        [{$oViewConf->getHiddenSid()}]
        <input type="hidden" name="cl" value="search">

        <!-- Suchfeld -->
        [{block name="dd_widget_header_search_form_inner"}]
            <div class="input-group">
                [{block name="header_search_field"}]
                    <input class="form-control" type="text" id="searchParamBottom" name="searchparam" value="[{$oView->getSearchParamForHtml()}]" placeholder="[{oxmultilang ident="SEARCH"}]">
                [{/block}]

                [{block name="dd_header_search_button"}]
                    <span class="input-group-btn">
						<button type="submit" id="suchbuttonbottom" class="btn btn-primary" title="[{oxmultilang ident="SEARCH_SUBMIT"}]">
							<i class="fa fa-search"></i>
						</button>
					</span>
                [{/block}]
            </div>
        [{/block}]
    </form>
[{/if}]

