[{capture name="myJSbottom"}]
    $(document).ready(function () {
    var options = {
    /**
    * auswertung der Usereingabe und absetzen des Http_GET Request an form.php
    * zuvor validierung der Sprachauswahl de als Standarteinstellung berücksichtigt.
    */
    url: function(phrase) {
    return "[{$oViewConf->getBaseDir()}]index.php?cl=dre_solrsuchecontroller&fnc=suche&phrase=" + phrase;
    },
    ajaxSettings: {
        dataType: "json"
    },
    listLocation: function (response) {
        return response.artikelobjekte; //response.response.docs
    },
    theme: "custom",
    /**
    * Value des Search Fields bis eingabe vorgenommen wird
    */
    //placeholder: "[{oxmultilang ident='SUCHBEGRIFF'}]",
    /**
    * Value des Search Fields, wenn eine Auswahl getroffen wurde
    */
    getValue: function(response){
        return response.oxarticles__oxtitle.value;
    },
    /**
    * Einstellung: Wo der Suchbegriff mit dem Ergebnis matched, wird dies hervorgehoben
    */
    highlightPhrase: true,
    /**
    * Zeitverzögerung bis die Abfrage abgeschickt wird bei Usereingabe
    */
    requestDelay: 100,
    /**
    * Anzahl der Mindestzeichen bis die Suche ausgelöst wird
    */
    minCharNumber: 3,
    /**
    * Objekt list Array von Objekten für die Auswahlliste
    */
    list:{
    maxNumberOfElements: 30,
    /**
    * Objekt für die Animation bei der Darstellung der Liste
    * */
    showAnimation: {
    type: "fade", //normal|slide|fade
    time: 100,
    callback: function(response) {
    }
    },
    /**
    * Trigger der ausgelöst wird, wenn die Liste geladen wird
    * */
    onLoadEvent:function(){
        $('#eac-container-searchParamBottom').removeClass('easy-autocomplete-container').addClass('easy-autocomplete-container-bottom')
    },
    /**
    *
    * Trigger der ausgelöst wird, wenn der User eine Auswahl getroffen hat
    *
    * */
    onClickEvent: function () {
    }
    },
    /**
    *
    * Template Objekt als Array von Objekten
    *
    * */
    template: {
    type: "custom",
    method: function(value, item) {
    var price = null;
    var formprice = null;
    if(parseInt(item.preis) > 0){
    formprice = item.preis+'€';
    }else{
    formprice = accounting.formatMoney(item.oxarticles__oxprice.value, "€", 2, ".", ",","%v %s");
    }
    var string = "<a href='" + item.link +  "'><img src='" + item.pic + "' width='50px' height='50px'>"+
        "<span class='ArtTitle'>" + value + "</span>" +
        "<br/><span class='small' style='color:#777;'>[{oxmultilang ident="PRODUCT_NO" suffix="COLON"}] " + item.artnum + "</span>"+
        "<span class='pull-right'>" + formprice + "</span></a>";
    return string;
    }
    }
    };
    $("#searchParamBottom").easyAutocomplete(options);
    });
[{/capture}]
[{oxscript add=$smarty.capture.myJSbottom}]