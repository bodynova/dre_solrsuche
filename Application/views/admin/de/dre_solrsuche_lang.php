<?php
$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
	'charset'                                    => 'UTF-8',
	'SHOP_MODULE_blFeSearchEnabled'              => 'Suche im Frontend aktivieren',
    'SHOP_MODULE_GROUP_dre-search_main'          => 'Grundeinstellungen',
    'SHOP_MODULE_dre-search-col'                 => 'Such Kern',
    'SHOP_MODULE_dre-search-url'                 => 'Such Url',
);
