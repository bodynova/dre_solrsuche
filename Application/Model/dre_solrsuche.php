<?php

namespace Bender\dre_SolrSuche\Application\Model;

use OxidEsales\Eshop\Core\Registry;

class dre_solrsuche extends \OxidEsales\Eshop\Application\Model\Search {

    /**
     * Hole die Suchurl und den eingestellten Knoten aus dem Shop-Backend
    */
    public function getSearchUrl(){
        $searchurl = Registry::getConfig()->getConfigParam('dre-search-url');
        $searchcol = Registry::getConfig()->getConfigParam('dre-search-col');
        return $searchurl . $searchcol . '/select';
    }

    /**
     * Suche für genaue Ergebnisse wie Artikelnummern
     */
    public function solrsuche($searchkey)
    {

        $suchresult = file_get_contents($this->getSearchUrl() . '?q=' . urlencode($searchkey) . '&indent=on&wt=json');
        $sucherg = json_decode($suchresult);
        $arrArtikelIds = array();
        foreach ($sucherg->response->docs as $artikel) {
            $arrArtikelIds[] = $artikel->id;
        }
        return $arrArtikelIds;
    }

    /**
     * Suche für genaue Ergebnisse wie Artikelnummern
     */
    public function solrsucheArtnum($searchkey)
    {

        $suchresult = file_get_contents($this->getSearchUrl() . '?q=childartnums_txt:' . urlencode($searchkey) . '&indent=on&wt=json');
        $sucherg = json_decode($suchresult);
        $arrArtikelIds = array();
        foreach ($sucherg->response->docs as $artikel) {
            $arrArtikelIds[] = $artikel->id;
        }
        return $arrArtikelIds;
    }

    /**
     * Suche mit erhöhter Fehlertoleranz
     */
    public function solrfuzzysuche($searchkey)
    {
        $suchresult = file_get_contents($this->getSearchUrl() . '?q=' . urlencode($searchkey) . '~&indent=on&wt=json');
        $sucherg = json_decode($suchresult);
        $arrArtikelIds = array();
        foreach ($sucherg->response->docs as $artikel) {
            $arrArtikelIds[] = $artikel->id;
        }
        return $arrArtikelIds;
    }

    /**
     * Shop eigene SQL Suche um Solr Eigenschaften erweitert
     */
    public function getSearchArticles($sSearchParamForQuery = false, $sInitialSearchCat = false, $sInitialSearchVendor = false, $sInitialSearchManufacturer = false, $sSortBy = false)
    {
        /* sets active page
        $this->iActPage = (int)\OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('pgNr');
        $this->iActPage = ($this->iActPage < 0) ? 0 : $this->iActPage;

        $iNrofCatArticles = $this->getConfig()->getConfigParam('iNrofCatArticles');
        $iNrofCatArticles = $iNrofCatArticles ? $iNrofCatArticles : 10;
        */
        $oArtList = null;
        $arrArtikelIds = $this->solrsucheArtnum($sSearchParamForQuery);
        if(count($arrArtikelIds) == 1){
            $oArtList = oxNew(\OxidEsales\Eshop\Application\Model\ArticleList::class);
            $oArtList->loadIdsSort($arrArtikelIds, $arrArtikelIds);
            return $oArtList;
            //$arrArtikelIds = $this->solrfuzzysuche($sSearchParamForQuery);
        }else{
            $arrArtikelIds = $this->solrsuche($sSearchParamForQuery);
        }

        if (count($arrArtikelIds) == 0) {
            $arrArtikelIds = $this->solrfuzzysuche($sSearchParamForQuery);
        }

        $oArtList = oxNew(\OxidEsales\Eshop\Application\Model\ArticleList::class);
        $oArtList->loadIdsSort($arrArtikelIds, $arrArtikelIds);
        return $oArtList;
    }

    /**
     * Anzeige der gefunden Treffer auf der Ergebnisseite
    */
    public function getSearchArticleCount($sSearchParamForQuery = false, $sInitialSearchCat = false, $sInitialSearchVendor = false, $sInitialSearchManufacturer = false)
    {
        $iCnt = 0;
        $oArtList = null;
        $arrArtikelIds = $this->solrsucheArtnum($sSearchParamForQuery);
        if (count($arrArtikelIds) == 1) {
            $iCnt = count($arrArtikelIds);
            return $iCnt;
            //$arrArtikelIds = $this->solrfuzzysuche($sSearchParamForQuery);
        } else {
            $arrArtikelIds = $this->solrsuche($sSearchParamForQuery);
        }

        if (count($arrArtikelIds) == 0) {
            $arrArtikelIds = $this->solrfuzzysuche($sSearchParamForQuery);
        }
        /*
        $arrArtikelIds = $this->solrsuche($sSearchParamForQuery);
        if (count($arrArtikelIds) == 0) {
            $arrArtikelIds = $this->solrfuzzysuche($sSearchParamForQuery);
        }
        */
        $iCnt = count($arrArtikelIds);
        return $iCnt;
    }
}