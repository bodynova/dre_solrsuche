<?php
namespace Bender\dre_SolrSuche\Application\Model;

class dre_ArticleList extends \OxidEsales\Eshop\Application\Model\ArticleList{

    /**
     * Load the list by article ids
     *
     * @param array $aIds Article ID array
     *
     * @param null $sort
     * @return void ;
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */
    public function loadIdsSort($aIds, $sort = null)
    {
        if (!count($aIds)) {
            $this->clear();

            return;
        }

        $oBaseObject = $this->getBaseObject();
        $sArticleTable = $oBaseObject->getViewName();
        $sArticleFields = $oBaseObject->getSelectFields();

        $oxIdsSql = implode(',', \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quoteArray($aIds));

        $sSelect = "select $sArticleFields from $sArticleTable ";
        $sSelect .= "where $sArticleTable.oxid in ( " . $oxIdsSql . " ) and ";
        $sSelect .= $oBaseObject->getSqlActiveSnippet();
        if($sort){
            $sSelect .= " Order By FIELD( $sArticleTable.oxid, '" . implode('\' ,\'', $sort) . "')";
        }

        #echo $sSelect;
        #die();

        $this->selectString($sSelect);
    }
}