<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
	'id'           => 'dre_solrsuche',
	'title'        => '<img src="../modules/bender/dre_solrsuche/out/img/favicon.ico" title="Body Solr Suche" />ody Solr Suche',
	'description'  => 'Bodynova Solr Such Composer Modul',
	'thumbnail'    => 'out/img/logo_bodynova.png',
	'version'      => '2.0.0',
	'author'       => 'André Bender',
	'url'          => 'https://bitbucket.org/bodynova/dre_solrsuche',
	'email'        => 'a.bender@bodynova.de',
	'extend'       => array(
		\OxidEsales\Eshop\Core\Config::class => \Bender\dre_SolrSuche\Core\Config::class,
        \OxidEsales\Eshop\Application\Model\Search::class => \Bender\dre_SolrSuche\Application\Model\dre_solrsuche::class,
        \OxidEsales\Eshop\Application\Model\ArticleList::class => \Bender\dre_SolrSuche\Application\Model\dre_ArticleList::class,
	),
	'controllers' => array(
        'dre_solrsuchecontroller'
        => \Bender\dre_SolrSuche\Application\Controller\dre_SolrSuchController::class,
	),
	
	'events'=> array(
	),
	'templates' => array(
	    'dre_autocomplete.tpl' => 'bender/dre_solrsuche/Application/views/tpl/dre_autocomplete.tpl',
        'dre_autocomplete_bottom.tpl' => 'bender/dre_solrsuche/Application/views/tpl/dre_autocomplete_bottom.tpl',
        'dre_autocomplete_top.tpl' => 'bender/dre_solrsuche/Application/views/tpl/dre_autocomplete_top.tpl',
        'dre_searchresult.tpl' => 'bender/dre_solrsuche/Application/views/tpl/dre_searchresult.tpl',
	),
	'blocks' => array(
		array(
			'template'  => 'layout/base.tpl',
			'block'     => 'base_style',
			'file'      => 'Application/views/blocks/dre_base_style.tpl'
		),
		array(
			'template'  => 'layout/base.tpl',
			'block'     => 'base_js',
			'file'      => 'Application/views/blocks/dre_base_js.tpl'
		),
		array(
			'template'  => 'widget/header/search.tpl',
			'block'     => 'widget_header_search_form',
			'file'      => 'Application/views/blocks/dre_solrsearch.tpl'
		),
        array(
            'template'  => 'widget/header/search_bottom.tpl',
            'block'     => 'widget_header_search_form_bottom',
            'file'      => 'Application/views/blocks/dre_solrsearch_bottom.tpl'
        ),
        array(
            'template'  => 'widget/header/search_top.tpl',
            'block'     => 'widget_header_search_form_top',
            'file'      => 'Application/views/blocks/dre_solrsearch_top.tpl'
        ),
	),
	'settings' => array(
		array(
			'group' => 'dre_solrSearch',
			'name'  => 'blFeSearchEnabled',
			'type'  => 'bool',
			'value' => true
		),
        array(
            'group' => 'dre-search_main',
            'name' => 'dre-search-col',
            'type' => 'str',
            'value' => 'mycol1',
        ),
        array(
            'group' => 'dre-search_main',
            'name' => 'dre-search-url',
            'type' => 'str',
            'value' => 'https://sales.bodynova.com:8983/solr/',
        ),
	),
);
