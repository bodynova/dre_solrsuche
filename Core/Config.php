<?php
namespace Bender\dre_SolrSuche\Core;

use OxidEsales\Eshop\Core\Registry;

class Config extends Config_parent{
	/**
	 * Rückgabe, ob die Suche im Frontend aktiviert sein soll
	 * @return bool
	 */
	public function SucheFrontendAktiv()
	{
		return (bool) $this->getShopConfVar('blFeSearchEnabled', null, 'module:dre_solrsuche') ?: true;
	}
}
